// please run a command below this (1st)
// $ go mod init checkopenport
package main

import (
	"fmt"
	"net"
	"time"
)

func main() {
	ports := []string{"22", "80"}

	raw_connect("192.168.1.69", ports)
}

func raw_connect(host string, ports []string) {
	for _, port := range ports {
		timeout := time.Second
		conn, err := net.DialTimeout("tcp", net.JoinHostPort(host, port), timeout)
		if err != nil {
			fmt.Println("Connecting error:", err)
		}
		if conn != nil {
			defer conn.Close()
			fmt.Println("Opened", net.JoinHostPort(host, port))
		}
	}
}
